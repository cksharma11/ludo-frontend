import React from "react";
import ReactDOM from "react-dom";
import GamePage from "./component/GamePage";
import "./game_page.css";

ReactDOM.render(<GamePage />, document.getElementById("root"));
