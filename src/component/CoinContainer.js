import React from "react";

const CoinContainer = props => {
  return (
    <div className="coin_container" id={props.playerId}>
    <div className="container-border">
      <div className="container_row">
        <div id={props.color + "_coin_100"} className={"coin " + props.color} />
        <div id={props.color + "_coin_200"} className={"coin " + props.color} />
      </div>
      <div className="container_row">
        <div id={props.color + "_coin_300"} className={"coin " + props.color} />
        <div id={props.color + "_coin_400"} className={"coin " + props.color} />
      </div>
      </div>
    </div>
  );
};

export default CoinContainer;
