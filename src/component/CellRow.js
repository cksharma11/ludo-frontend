import React from "react";
import Cell from "./Cell";

const CellRow = props => {
  let cellId = props.cellId - 1;
  let color = props.color;
  let specialCells = props.specialCells;
  let className = props.className;
  let containerClass = props.containerClass;

  const getClassName = cellId =>
    "cell ".concat(specialCells.includes(cellId) ? color : "");

  return (
    <div className={containerClass}>
      <div className={className}>
        <Cell id={++cellId} className={getClassName(cellId)} />
        <Cell id={++cellId} className={getClassName(cellId)} />
        <Cell id={++cellId} className={getClassName(cellId)} />
      </div>
      <div className={className}>
        <Cell id={++cellId} className={getClassName(cellId)} />
        <Cell id={++cellId} className={getClassName(cellId)} />
        <Cell id={++cellId} className={getClassName(cellId)} />
      </div>
      <div className={className}>
        <Cell id={++cellId} className={getClassName(cellId)} />
        <Cell id={++cellId} className={getClassName(cellId)} />
        <Cell id={++cellId} className={getClassName(cellId)} />
      </div>
      <div className={className}>
        <Cell id={++cellId} className={getClassName(cellId)} />
        <Cell id={++cellId} className={getClassName(cellId)} />
        <Cell id={++cellId} className={getClassName(cellId)} />
      </div>
      <div className={className}>
        <Cell id={++cellId} className={getClassName(cellId)} />
        <Cell id={++cellId} className={getClassName(cellId)} />
        <Cell id={++cellId} className={getClassName(cellId)} />
      </div>
      <div className={className}>
        <Cell id={++cellId} className={getClassName(cellId)} />
        <Cell id={++cellId} className={getClassName(cellId)} />
        <Cell id={++cellId} className={getClassName(cellId)} />
      </div>
    </div>
  );
};

export default CellRow;
