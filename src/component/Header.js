import React from "react";

const Header = props => {
  return (
    <header>
      <div id="title">{props.title}</div>
      <div id="player_name">{props.playerName}</div>
    </header>
  );
};

export default Header;
