import React from "react";

const Coin = props => {
  return <div class={"coin_symbol dark_" + props.color} />;
};

export default Coin;
